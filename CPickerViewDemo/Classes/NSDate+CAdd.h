//
//  NSDate+CAdd.h
//  CPickerViewDemo
//
//  Created by InitialC on 2017/8/11.
//  Copyright © 2017年 InitialC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (CAdd)
/** 获取当前的时间 */
+ (NSString *)currentDateString;

@end
