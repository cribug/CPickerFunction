//
//  CTextField.h
//  CPickerViewDemo
//
//  Created by Mr.C on 2017/8/11.
//  Copyright © 2017年 Mr.C. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CTapAcitonBlock)();
typedef void(^CEndEditBlock)(NSString *text);

@interface CTextField : UITextField
/** textField 的点击回调 */
@property (nonatomic, copy) CTapAcitonBlock tapAcitonBlock;
/** textField 结束编辑的回调 */
@property (nonatomic, copy) CEndEditBlock endEditBlock;

@end
